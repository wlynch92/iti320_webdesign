<?xml version="1.0" encoding="UTF-8" ?>
<!--
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
-->
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="/atlas/default.css" />
        <link rel="stylesheet" type="text/css" href="/atlas/print.css" media="print" />
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" /> 
<?php
echo "\t<title>Atlas Solutions";
if($_POST['title']){
    echo " - ".$_POST['title'];
}
echo "</title>\n";
?>
    </head>
<?php include_once("analyticstracking.php") ?>    
    <body>
        <div id="wrapper">
            <div id="header">
                <img src="/atlas/img/atlas-logo-trans.fw.png" hspace="30" vspace="52" usemap="#Map" alt="Atlas Solutions"/>
                <map name="Map" id="Map">
                    <area shape="circle" coords="44,43,43" href="/atlas" alt="Atlas Solutions"/>
                </map>
                <ul id="nav">
                    <li>
                        <a href="/atlas">Home</a>
                    </li>
                    <li>
                        <a>About</a>
                        <ul>
                            <li><a href="/atlas/about/events.php">Events</a></li>
                            <li><a href="/atlas/about/history.php">History</a></li>
                            <li><a href="/atlas/about/jobs.php">Jobs</a></li>
                            <li><a href="/atlas/about/staff.php">Staff</a></li>
                        </ul>
                    </li>
                    <li>
                        <a>Services</a>
                        <ul>
                            <li><a href="/atlas/services/home.php">Home</a></li>
                            <li><a href="/atlas/services/business.php">Business</a></li>
                        </ul>                        
                    <li><a href="/atlas/contact.php">Contact</a></li>
                    <li><a href="/atlas/search.php">Search</a></li>
                </ul>
            </div>
            <div id="printHeader">
                <img src="/atlas/img/atlas-logo-print.fw.png" alt=""/>
            </div>
            <div id="hours">
                <h3>Hours</h3>
                <p>
                Monday      9-6
                <br/>
                Tuesday     9-6
                <br/>
                Wednesday   9-6
                <br/>
                Thursday    9-6
                <br/>
                Friday      9-5
                <br/>
                Saturday    10-5
                </p>
            </div>
